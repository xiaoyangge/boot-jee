package com.gitee.hermer.boot.jee.upms.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

@ConfigurationProperties(prefix = "com.boot.jee.upms.shiro")  
public class UpmsShiroProperties {
	
	private String login;
	
	private String success;
	private String unauthorized;
	private String hash;
	
	private String logout;
	
	
	
	
	
	
	public String getLogout() {
		return StringUtils.defaultIfEmpty(logout, "/logout");
	}
	public void setLogout(String logout) {
		this.logout = logout;
	}
	public String getHash() {
		return StringUtils.defaultIfEmpty(hash, "md5");
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getLogin() {
		return StringUtils.defaultIfEmpty(login, "/login");
	}
	public String getSuccess() {
		return StringUtils.defaultIfEmpty(success, "/success");
	}
	public String getUnauthorized() {
		return StringUtils.defaultIfEmpty(unauthorized, "/unauthorized");
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public void setUnauthorized(String unauthorized) {
		this.unauthorized = unauthorized;
	}
	
	
	
	
	
	
	
}
