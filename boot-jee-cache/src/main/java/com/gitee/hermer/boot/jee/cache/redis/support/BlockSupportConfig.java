package com.gitee.hermer.boot.jee.cache.redis.support;


public class BlockSupportConfig {
    private int timeOutMillis = 60000; 
    private int timeLockMillis = 60000;
    private int stripes = 1024;
    private int timeWaitMillis = 300;
    private boolean block = false;

    public int getTimeOutMillis() {
        return timeOutMillis;
    }

    public void setTimeOutMillis(int timeOutMillis) {
        this.timeOutMillis = timeOutMillis;
    }

    public int getTimeLockMillis() {
        return timeLockMillis;
    }

    public void setTimeLockMillis(int timeLockMillis) {
        this.timeLockMillis = timeLockMillis;
    }

    public int getStripes() {
        return stripes;
    }

    public void setStripes(int stripes) {
        this.stripes = stripes;
    }

    public int getTimeWaitMillis() {
        return timeWaitMillis;
    }

    public void setTimeWaitMillis(int timeWaitMillis) {
        this.timeWaitMillis = timeWaitMillis;
    }

    public boolean isBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }
}
