package com.gitee.hermer.boot.jee.upms.mapping;

import java.lang.reflect.Method;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.gitee.hermer.boot.jee.api.BaseControllerHandler;
import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.collection.StringCache;
import com.gitee.hermer.boot.jee.commons.collection.list.ListUtils;
import com.gitee.hermer.boot.jee.commons.dict.BootProperties;
import com.gitee.hermer.boot.jee.commons.reflect.ClassUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

public class BootMappingHandler extends RequestMappingHandlerMapping {
	
	
	@Autowired
	private BootProperties properties;

	@Override 
	protected void registerHandlerMethod(Object handler, Method method, RequestMappingInfo mapping) 
	{ 
		
		if(ClassUtils.isAssignable(BaseControllerHandler.class, method.getDeclaringClass())){
			List<String>  patterns = ListUtils.convertToList(mapping.getPatternsCondition().getPatterns());
			List<String> temps = CollectionUtils.newArrayList();
			
			String suffix = null;
			try { suffix = properties.getDictValueString("com.boot.jee.upms.mapping.api.suffix"); } catch (Exception e) { }
			for (String pattern : patterns) {
				temps.add(new StringCache(appendSymbol('/',StringUtils.defaultIfEmpty(suffix,"/apirestful"))).append(appendSymbol('/',pattern)).toString());
			}
			PatternsRequestCondition condition = new PatternsRequestCondition(temps.toArray(new String[]{}));
			mapping = new RequestMappingInfo(mapping.getName(), condition, mapping.getMethodsCondition(), 
					mapping.getParamsCondition(), mapping.getHeadersCondition(), mapping.getConsumesCondition(), mapping.getProducesCondition(), mapping.getCustomCondition());
		}else if(ClassUtils.isAssignable(com.gitee.hermer.boot.jee.web.BaseControllerHandler.class, method.getDeclaringClass())){
			List<String>  patterns = ListUtils.convertToList(mapping.getPatternsCondition().getPatterns());
			List<String> temps = CollectionUtils.newArrayList();
			
			String suffix = null;
			try { suffix = properties.getDictValueString("com.boot.jee.upms.mapping.web.suffix"); } catch (Exception e) { }
			for (String pattern : patterns) {
				temps.add(new StringCache(appendSymbol('/',StringUtils.defaultIfEmpty(suffix,"/webview"))).append(appendSymbol('/',pattern)).toString());
			}
			PatternsRequestCondition condition = new PatternsRequestCondition(temps.toArray(new String[]{}));
			mapping = new RequestMappingInfo(mapping.getName(), condition, mapping.getMethodsCondition(), 
					mapping.getParamsCondition(), mapping.getHeadersCondition(), mapping.getConsumesCondition(), mapping.getProducesCondition(), mapping.getCustomCondition());
		}
		super.registerHandlerMethod(handler, method, mapping); 
	}
	
	private String appendSymbol(char symbol,String pattern){
		return pattern.charAt(0) == symbol?pattern:symbol+pattern;
	}
}
