package com.gitee.hermer.boot.jee.cache;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.gitee.hermer.boot.jee.commons.Id.generator.IdWorker;
import com.gitee.hermer.boot.jee.commons.bean.utils.BeanLocator;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@EnableAutoConfiguration(exclude = {
		DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class,
		HibernateJpaAutoConfiguration.class})
@PropertySource(value="classpath:/boot-cache.properties")
public class RedisCacheServiceTest extends BeanLocator{

	private String ID = StringUtils.getRandomString(5);
	
	private String key = null;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private RedisTemplate redisTemplate;
	
	@Before
	public void testBefore(){
		try {
			ctx = context;
			BootCache.clear(ID);
			redisTemplate.execute(new RedisCallback() {
				@Override
				public Object doInRedis(RedisConnection connection) throws DataAccessException {
					connection.flushDb();
					return "ok";
				}
			});
			key = new IdWorker().nextId()+"";
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testCache(){
		BootCache.save(ID, key, StringUtils.getRandomString(10));
		assertTrue("Cache保存错误！", BootCache.size(ID) == 1);
		BootCache.remove(ID, key);
		
		assertTrue("Cache删除错误！", BootCache.size(ID) == 0);
		assertTrue("Cache删除错误！", BootCache.get(ID, key) == null);
	}
	
	
	@Test
	public void testCacheClear(){
		BootCache.save(ID, key, StringUtils.getRandomString(10));
		assertTrue("Cache保存错误！", BootCache.size(ID) == 1);
		assertNotNull(BootCache.get(ID, key));
		BootCache.clear(ID);
		
		assertTrue("Cache删除错误！", BootCache.size(ID) == 0);
		assertTrue("Cache删除错误！", BootCache.get(ID, key) == null);
		
		
	}
	

	
}
