package com.gitee.hermer.boot.jee.generate;

import com.gitee.hermer.boot.jee.generate.controller.GenerateController;
import com.gitee.hermer.boot.jee.generate.orm.ibatis.IbatisGenerateMapper;
import com.gitee.hermer.boot.jee.generate.service.GenerateService;

public class GenerateUtils {
	
	public static void generate(){
		IbatisGenerateMapper.generate();
		GenerateService.generate();
		GenerateController.generate();
	}

}
