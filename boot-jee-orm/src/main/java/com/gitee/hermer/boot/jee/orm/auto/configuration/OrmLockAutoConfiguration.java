package com.gitee.hermer.boot.jee.orm.auto.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.orm.ibatis.plugins.OptimisticLockerPlugin;
import com.gitee.hermer.boot.jee.orm.properties.OrmLockProperties;



@Configuration    
@EnableConfigurationProperties(value = {OrmLockProperties.class})   
@ConditionalOnProperty(prefix = "com.boot.jee.orm.optimistic.lock", value = "enable",havingValue="true",matchIfMissing = false)  
public class OrmLockAutoConfiguration extends UtilsContext {
	
	
	@Autowired
	private OrmLockProperties lockProperties;
	
	@Bean
	@ConditionalOnMissingBean(OptimisticLockerPlugin.class) 
	@ConditionalOnProperty(prefix = "com.boot.jee.orm",value = "factory", havingValue = "mybatis", matchIfMissing = true)    
	public OptimisticLockerPlugin getOptimisticLockerPlugin() {
		Properties properties = new Properties();
		properties.setProperty("versionColumn", lockProperties.getVersionColumn());
		properties.setProperty("versionField", lockProperties.getVersionField());
		OptimisticLockerPlugin lockerPlugin = new OptimisticLockerPlugin();
		lockerPlugin.setProperties(properties);
		return lockerPlugin;
	}



}
