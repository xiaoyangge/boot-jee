## Boot-Jee项目简介

- Boot-Jee是Java语言的分布式系统架构。 使用Spring整合开源框架。
- 使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性。
- 封装监控、路由映射、基本公用数据操作、通信，目的是为了让开发者更加关注业务实现。

### 组织结构

``` lua
Boot-Jee
├── boot-jee-api -- 封装对@RestController的增强Handler,主要功能：封装对外的基础数据操作，统一的异常处理，集成Swagger
├── boot-jee-cache -- 封装对ORM层的二级数据缓存，执行查询EhCache > RedisCache > Read DB
├── boot-jee-data-cache -- 封装对ORM层的二级数据缓存，基于spring data redis，bean由spring托管，执行查询EhCache > RedisCache > Read DB
├── boot-jee-commons -- 封装公用的帮助类、反射、线程池、数据字典、HTTP请求、二维码生成、IO操作、日志等等。
├── boot-jee-io -- 封装框架的通讯协议，主要提供activemq、socket可应用场景点对点通讯、订阅、队列
├── boot-jee-metrics -- 封装对整个系统所依赖系统级监控、性能指标，主要对应存活检测/health、指标监控/metrics
├── boot-jee-ngrok -- 封装ngrok客户端实现，用于将本地端口暴露外网，一般用于开发阶段，应用场景比如调试微信公众号、奇门接口，临时用该服务免费
├── boot-jee-parent -- Maven父类工程定义子类pom parent
├── boot-jee-orm -- 封装ORM层，主要功能：慢查询预警记录，乐观锁，数据分页，基础数据操作
├── boot-jee-service -- 封装基础操作，依赖于ORM模块
├── boot-jee-schedule -- 分布式调度平台
├── boot-jee-generate -- 封装代码生成器，提供dao，service，controller，html的模板生成
├── boot-jee-upms -- 封装网关层，主要功能：权限控制，OAuth2.0，及改写Spring中mapping映射逻辑将继承于api、web模块分别路由
└── boot-jee-web -- 封装对@Controller的增强Handler,主要功能：封装对外的基础数据操作，统一的异常处理
```

### 框架架构
![框架架构](https://gitee.com/uploads/images/2017/1021/161223_895ecb6b_24602.png "图片1.png")

### 框架优势
![框架优势](https://gitee.com/uploads/images/2017/1021/161339_3c35b41e_24602.jpeg "图片2.jpg")


### 配置文件说明

- 基础配置`application.properties`

``` lua
# api module
com.boot.jee.api.swagger.enable = true
# io module
com.boot.jee.io.enable = true
com.boot.jee.io.factory = activemq,socketio
com.boot.jee.io.activemq.url=http://localhost:6161/
com.boot.jee.io.activemq.userName=admin
com.boot.jee.io.activemq.passWord=admin
com.boot.jee.io.socket.host=127.0.0.1
com.boot.jee.io.socket.port=8091
# metrice module
com.boot.jee.metrice.enable=true
com.boot.jee.metrice.factory=redis
# ngrok module
com.boot.jee.ngrok.enable=true
com.boot.jee.ngrok.serevr-address=tunnel.qydev.com
com.boot.jee.ngrok.server-port=4443
com.boot.jee.ngrok.subdomain=hermer
# 增强属性 协议支持http tcp udp
com.boot.jee.ngrok.proto=http
com.boot.jee.ngrok.remote-port=8080
com.boot.jee.ngrok.http-auth=user:password
# orm module
com.boot.jee.orm.factory = hibernate,mybatis
com.boot.jee.orm.mybatis.config-locations=classpath:mybatis/mybatis-config.xml
com.boot.jee.orm.mybatis.mapper-locations=classpath:mybatis/mapper/*.xml
com.boot.jee.orm.optimistic.lock.enable=true
com.boot.jee.orm.optimistic.lock.version-column=version
com.boot.jee.orm.optimistic.lock.version-field=version
com.boot.jee.orm.page.offsetAsPageNum=true
com.boot.jee.orm.page.rowBoundsWithCount=true
com.boot.jee.orm.page.reasonable=true
com.boot.jee.orm.page.dialect=mysql
com.boot.jee.orm.slow.sql.enable=true
com.boot.jee.orm.slow.executeTimeThreshold=1000
# upms module
com.boot.jee.upms.mapping.api.suffix=/api
com.boot.jee.upms.mapping.web.suffix=/web
com.boot.jee.upms.shiro.login=/login
com.boot.jee.upms.shiro.success=/success
com.boot.jee.upms.shiro.unauthorized=/unauthorized
com.boot.jee.upms.shiro.hash=md5
com.boot.jee.upms.shiro.logout=/logout
com.boot.jee.upms.shiro.cache.enable=true
com.boot.jee.upms.shiro.cache.prefix=boot-cache-shiro
com.boot.jee.upms.shiro.cache.session-timeout=3600
# web module
com.boot.jee.web.404.page=error/404
com.boot.jee.web.500.page=error/500
com.boot.jee.web.thymeleaf.prefix = classpath:/templates/
com.boot.jee.web.thymeleaf.suffix = .html
com.boot.jee.web.thymeleaf.mode = HTML5  
com.boot.jee.web.thymeleaf.encoding = UTF-8  
com.boot.jee.web.thymeleaf.cache = false
com.boot.jee.web.thymeleaf.content-type = text/html


```
- 缓存配置`boot-cache.properties`

``` lua
cache.broadcast=redis

cache.L1.provider_class=ehcache
cache.L2.provider_class=redis

cache.serialization = fst

## connection
# single  host  127.0.0.1
# sharded host  redis://password@127.0.0.1:6379/0  \u591A\u4E2A\u7528\u9017\u53F7\u5206\u5272
# cluster host  127.0.0.1:6379,127.0.0.1:6380,127.0.0.1:6381
redis.host = localhost
redis.port = 6379
redis.timeout = 2000
redis.password = paiuredis
redis.database = 1
# redis \u90E8\u7F72\u7B56\u7565 single \u5355\u673A; cluster \u96C6\u7FA4; sharded \u5206\u724
redis.policy = single
redis.namespace = boot-cache
redis.channel_name = cache_channel

## properties
redis.maxTotal = -1
redis.maxIdle = 2000
redis.maxWaitMillis = 100
redis.minEvictableIdleTimeMillis = 864000000
redis.minIdle = 1000
redis.numTestsPerEvictionRun = 10
redis.lifo = false
redis.softMinEvictableIdleTimeMillis = 10
redis.testOnBorrow = true
redis.testOnReturn = false
redis.testWhileIdle = false
redis.timeBetweenEvictionRunsMillis = 300000
redis.blockWhenExhausted = true

#########################################
# Ehcache configuration
#########################################

ehcache.name=boot-cache
ehcache.configXml=ehcache.xml
```

- 代码生成配置`generate.properties`

``` lua
com.boot.jee.orm.entity.package.name= com.test.domain
com.boot.jee.orm.dao.package.name= com.test.dao
com.boot.jee.orm.entity.class.name= SortingPort
com.boot.jee.orm.entity.table.name=tt_sorting_port
com.boot.jee.service.package.name= com.test.service.impl
com.boot.jee.controlle.package.name= com.test.controlle.impl
```


## 版权声明
Boot-Jee使用 [Apache License 2.0][] 协议.

## 分支说明
[master][] 未拆分版本.

## License
Boot-Jee is released under version 2.0 of the [Apache License][].

[Apache License 2.0]: http://www.apache.org/licenses/LICENSE-2.0
[master]: https://gitee.com/hermer/boot-jee/tree/master
[Apache License]: http://www.apache.org/licenses/LICENSE-2.0