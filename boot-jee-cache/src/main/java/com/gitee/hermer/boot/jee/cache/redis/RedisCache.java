package com.gitee.hermer.boot.jee.cache.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitee.hermer.boot.jee.cache.Cache;
import com.gitee.hermer.boot.jee.cache.CacheException;
import com.gitee.hermer.boot.jee.cache.BootCache;
import com.gitee.hermer.boot.jee.cache.utils.SerializationUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class RedisCache implements Cache {


    private String locksPattern = "%s:lock:";
    private String locksNamePattern = locksPattern + "*";
    private String lockPattern = locksPattern + "%s"; 
    private static byte[] NX = "NX".getBytes(); 
    private static byte[] XX = "XX".getBytes();

    private static byte[] EX = "EX".getBytes();

    private static byte[] PX = "PX".getBytes();


    private final static Logger log = LoggerFactory.getLogger(RedisCache.class);

    
    protected byte[] region2;
    protected String region;
    protected RedisCacheProxy redisCacheProxy;

    public RedisCache(String region, RedisCacheProxy redisCacheProxy) {
        if (region == null || region.isEmpty())
            region = "_"; 

        region = getRegionName(region);
        this.redisCacheProxy = redisCacheProxy;
        this.region = region;
        this.region2 = region.getBytes();
    }

    
    private String getRegionName(String region) {
        String nameSpace = BootCache.getConfig().getProperty("redis.namespace", "");
        if (nameSpace != null && !nameSpace.isEmpty()) {
            region = nameSpace + ":" + region;
        }
        return region;
    }

    protected byte[] getKeyName(Object key) {
        if (key instanceof Number)
            return ("I:" + key).getBytes();
        else if (key instanceof String || key instanceof StringBuilder || key instanceof StringBuffer)
            return ("S:" + key).getBytes();
        return ("O:" + key).getBytes();
    }

    @Override
	public Object get(Object key) throws CacheException {
        if (null == key)
            return null;
        Object obj = null;
        try {
            byte[] keyName = getKeyName(key);
            byte[] b = redisCacheProxy.hget(region2, keyName);
            if (b != null) {
                obj = SerializationUtils.deserialize(b);
            } else if (redisCacheProxy.isBlock()) {
                byte[] lockKey = getLockKey(key);
                boolean locked = getLock(lockKey);
                if (locked || canReentrant(key)) {
                    return null;
                } else {
                    int timeLeft = redisCacheProxy.getTimeOutMillis();
                    while (timeLeft > 0) {
                        Thread.sleep(redisCacheProxy.getTimeWaitMillis());
                        timeLeft -= redisCacheProxy.getTimeWaitMillis();
                        b = redisCacheProxy.hget(region2, keyName);
                        if (b != null) {
                            obj = SerializationUtils.deserialize(b);
                            break;
                        } else {
                            
                            if (getLock(lockKey)) {
                                return null;
                            }
                        }
                        
                    }
                }

            }
        } catch (Exception e) {
            log.error("Error occured when get data from redis2 cache", e);
            if (e instanceof IOException || e instanceof NullPointerException)
                evict(key);
        }
        return obj;
    }

    private boolean canReentrant(Object key) {
        
        try {
            String value = redisCacheProxy.get(getLockKeyString(key));
            if (value != null) {
                long oriThreadId = Long.parseLong(value);
                return oriThreadId == Thread.currentThread().getId();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return false;
    }

    private boolean getLock(byte[] lockKey) {
        return getLock(lockKey, String.valueOf(Thread.currentThread().getId()).getBytes());
    }

    private boolean getLock(byte[] lockKey, byte[] keyName) {
        return "OK".equals(redisCacheProxy.set(lockKey, keyName, NX, PX, redisCacheProxy.getTimeLockMillis()));
    }

    private void releaseLock(byte[] lockKey) {
        redisCacheProxy.del(lockKey);
    }

    private String getLockKeyString(Object key) {
        return String.format(lockPattern, region, key.hashCode() % redisCacheProxy.getStripes());
    }

    private byte[] getLockKey(Object key) {
        String keyName = getLockKeyString(key);
        return keyName.getBytes();
    }

    @Override
	public void put(Object key, Object value) throws CacheException {
        if (key == null)
            return;
        if (value == null)
            evict(key);
        else {
            try {
                redisCacheProxy.hset(region2, getKeyName(key), SerializationUtils.serialize(value));
                if (redisCacheProxy.isBlock()) {
                    releaseLock(getLockKey(key));
                }
            } catch (Exception e) {
                throw new CacheException(e);
            }
        }
    }

    @Override
	public void update(Object key, Object value) throws CacheException {
        put(key, value);
    }

    @Override
	public void evict(Object key) throws CacheException {
        if (key == null)
            return;
        try {
            redisCacheProxy.hdel(region2, getKeyName(key));
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    @Override
	@SuppressWarnings("rawtypes")
    public void evict(List keys) throws CacheException {
        if (keys == null || keys.size() == 0)
            return;
        try {
            int size = keys.size();
            byte[][] okeys = new byte[size][];
            for (int i = 0; i < size; i++) {
                okeys[i] = getKeyName(keys.get(i));
            }
            redisCacheProxy.hdel(region2, okeys);
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    @Override
	public List<String> keys() throws CacheException {
        try {
            return new ArrayList<>(redisCacheProxy.hkeys(region));
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    @Override
	public void clear() throws CacheException {
        try {
            redisCacheProxy.del(region2);
            if (redisCacheProxy.isBlock()) {
                Set<byte[]> keys = redisCacheProxy.keys(String.format(locksNamePattern, region).getBytes());
                if (!keys.isEmpty()) {
                    redisCacheProxy.del(keys.toArray(new byte[][]{}));
                }
            }
        } catch (Exception e) {
            throw new CacheException(e);
        }
    }

    @Override
	public void destroy() throws CacheException {
        this.clear();
    }

    @Override
    public void put(Object key, Object value, Integer expireInSec) throws CacheException {
        if (key == null)
            return;
        if (value == null)
            evict(key);
        else {
            try {
                redisCacheProxy.hset(region2, getKeyName(key), SerializationUtils.serialize(value), expireInSec);
                if (redisCacheProxy.isBlock()) {
                    releaseLock(getLockKey(key));
                }
            } catch (Exception e) {
                throw new CacheException(e);
            }
        }
    }

    @Override
    public void update(Object key, Object value, Integer expireInSec) throws CacheException {
        put(key, value, expireInSec);
    }
}
