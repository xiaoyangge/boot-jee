package com.gitee.hermer.boot.jee.metrice.indicator;

import java.util.Map;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;

import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.metrice.service.IHealthIndicator;

/**
 * 
 * @ClassName: BootJeeHealthIndicator
 * @Description: 指标监控
 * @author:  涂孟超
 * @date: 2017年10月6日 下午5:32:45
 */
public abstract class BootJeeHealthIndicator extends UtilsContext implements HealthIndicator,IHealthIndicator{


	@Override
	public Health health() {
		Health.Builder builder = new Health.Builder();
		try{
			Map<String, String> params = metrice();
			for (String key : params.keySet()) {
				builder.withDetail(key, params.get(key));
			}
			builder.up();
		}catch (Throwable  e) {
			error(e.getMessage(),e);
			builder.down();
		}
		return builder.build();
	}


}
