package com.gitee.hermer.boot.jee.upms.shiro.users.service.impl;

import org.springframework.stereotype.Service;
import com.gitee.hermer.boot.jee.service.impl.BaseServiceImpl;
import com.gitee.hermer.boot.jee.upms.shiro.users.domain.AccountRole;
import com.gitee.hermer.boot.jee.upms.shiro.users.dao.IAccountRoleDao;


@Service
public class AccountRoleServiceImpl extends BaseServiceImpl<AccountRole, Integer, IAccountRoleDao>{

}