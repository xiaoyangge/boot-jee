package com.gitee.hermer.boot.jee.cache.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import com.gitee.hermer.boot.jee.commons.dict.domain.DictProperties;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;


@ConfigurationProperties
public class BootCacheProperties extends DictProperties  implements EnvironmentAware{
	

	private String cacheBroadcast;
	private String cacheL1Provider;
	private String cacheL2Provider;
	private String cacheSerialization;
	private String redisHost;
	private String redisTimeOut;
	private String redisPassword;
	private String redisPolicy;
	private String redisDataBase;
	private String redisNamespace;
	private String redisChannelName;
	private String redisMaxTotal;
	private String redisMaxIdle;
	private String redisMaxWaitMillis;
	private String redisMinEvictableIdleTimeMillis;
	private String redisMinIdle;

	private String redisNumTestsPerEvictionRun;
	private String redisLifo;
	private String redisSoftMinEvictableIdleTimeMillis;
	private String redisTestOnBorrow;
	private String redisTestOnReturn;
	private String redisTestWhileIdle;
	private String redisTimeBetweenEvictionRunsMillis;
	private String redisblockWhenExhausted;
	
	
	private String redisPort;
	
	
	private String ehcacheName;
	private String ehcacheConfigXml;
	
	private Environment env;
	@Override
	public void setEnvironment(Environment environment) {
		this.env = environment;
	}
	public String getCacheBroadcast() {
		return env.getProperty("cache.broadcast");
	}
	public String getCacheL1Provider() {
		return env.getProperty("cache.L1.provider_class");
	}
	public String getCacheL2Provider() {
		return env.getProperty("cache.L2.provider_class");
	}
	public String getCacheSerialization() {
		return env.getProperty("cache.serialization");
	}
	public String getRedisHost() {
		return env.getProperty("redis.host");
	}
	public String getRedisTimeOut() {
		return env.getProperty("redis.timeout");
	}
	public String getRedisPassword() {
		return env.getProperty("redis.password");
	}
	public String getRedisPolicy() {
		return env.getProperty("redis.policy");
	}
	public String getRedisDataBase() {
		return env.getProperty("redis.database");
	}
	public String getRedisNamespace() {
		return env.getProperty("redis.namespace");
	}
	public String getRedisChannelName() {
		return StringUtils.defaultIfEmpty(env.getProperty("redis.channel_name"), "default");
	}
	public String getRedisMaxTotal() {
		return env.getProperty("redis.maxTotal");
	}
	public String getRedisMaxIdle() {
		return env.getProperty("redis.maxIdle");
	}
	public String getRedisMaxWaitMillis() {
		return env.getProperty("redis.maxWaitMillis");
	}
	public String getRedisMinEvictableIdleTimeMillis() {
		return env.getProperty("redis.minEvictableIdleTimeMillis");
	}
	public String getRedisMinIdle() {
		return env.getProperty("redis.minIdle");
	}
	public String getRedisNumTestsPerEvictionRun() {
		return env.getProperty("redis.numTestsPerEvictionRun");
	}
	public String getRedisLifo() {
		return env.getProperty("redis.lifo");
	}
	public String getRedisSoftMinEvictableIdleTimeMillis() {
		return env.getProperty("redis.softMinEvictableIdleTimeMillis");
	}
	public String getRedisTestOnBorrow() {
		return env.getProperty("redis.testOnBorrow");
	}
	public String getRedisTestOnReturn() {
		return env.getProperty("redis.testOnReturn");
	}
	public String getRedisTestWhileIdle() {
		return env.getProperty("redis.testWhileIdle");
	}
	public String getRedisTimeBetweenEvictionRunsMillis() {
		return env.getProperty("redis.timeBetweenEvictionRunsMillis");
	}
	public String getRedisblockWhenExhausted() {
		return env.getProperty("redis.blockWhenExhausted");
	}
	public String getRedisPort() {
		return env.getProperty("redis.port");
	}
	public String getEhcacheName() {
		return env.getProperty("ehcache.name");
	}
	public String getEhcacheConfigXml() {
		return env.getProperty("ehcache.configXml");
	}

}
